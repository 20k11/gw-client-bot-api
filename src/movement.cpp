#include <gwcba/agents.h>
#include <gwcba/inventory.h>
#include <gwcba/movement.h>
#include <gwcba/skillbar.h>

bool isTimeElapsed(std::chrono::time_point<std::chrono::steady_clock> timePoint, uint32_t timeoutInMilliSeconds) {
  auto now = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::milliseconds>(now - timePoint).count() > timeoutInMilliSeconds;
}

void move(GW::GamePos gamePos) {
  GW::GameThread::Enqueue([gamePos] {
    if (isPlayerLoadedOnMap()) GW::Agents::Move(gamePos.x, gamePos.y, gamePos.zplane);
  });
}

bool moveTo(GW::GamePos destinationPos, const std::function<bool()> &moveStepCallback, uint32_t timeoutInMilliSeconds) {
  bool destinationReached = false;
  GW::Agent *player = GW::Agents::GetPlayer();
  if (!isPlayerLoadedOnMap()) return false;

  move(destinationPos);

  auto start = std::chrono::high_resolution_clock::now();
  while (!destinationReached) {
    if (!moveStepCallback()) return false;

    if (player->GetAsAgentLiving()->GetIsDead()) return false;
    if (!isAgentMoving(player)) move(destinationPos);
    Sleep(50);
    float distance = GW::GetSquareDistance(player->pos, destinationPos);
    destinationReached = distance <= 25;

    if (isTimeElapsed(start, timeoutInMilliSeconds)) return false;
  }
  return true;
}

void moveToZone(GW::GamePos destination, GW::Constants::MapID mapToLoad) {
  GW::Constants::MapID currentMapId;
  do {
    currentMapId = GW::Map::GetMapID();
    move(destination);
    Sleep(50);
  } while (currentMapId != mapToLoad);
}
