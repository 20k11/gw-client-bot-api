#include <gwcba/skillbar.h>
#include <gwcba/agents.h>
#include <gwcba/movement.h>

void loadBuildTemplate(const std::string &buildTemplate) {
  if (GW::Map::GetInstanceType() != GW::Constants::InstanceType::Outpost) return;
  const char *codeChar = buildTemplate.c_str();
  GW::SkillbarMgr::LoadSkillTemplate(codeChar, 0);
}

bool useSkill(uint32_t skillSlot, GW::AgentID targetAgentId, uint32_t timeoutInMilliSeconds) {
  if (skillSlot < 1 || skillSlot > 8) return false;
  if (!GW::Agents::GetAgentByID(targetAgentId)) return false;
  if (!isPlayerLoadedOnMap()) return false;
  GW::Agent *player = GW::Agents::GetPlayer();
  if (player->GetAsAgentLiving()->GetIsDead()) return false;

  auto startUseSkill = std::chrono::high_resolution_clock::now();

  GW::Skillbar *skillbar = GW::SkillbarMgr::GetPlayerSkillbar();
  if (!skillbar || !skillbar->IsValid()) return false;
  if (skillbar->skills[skillSlot - 1].GetRecharge() != 0) return false;

  do {
    Sleep(150);
  } while (isPlayerLoadedOnMap() && player->GetAsAgentLiving()->GetIsCasting());

  GW::GameThread::Enqueue([skillSlot, targetAgentId]() -> void { GW::SkillbarMgr::UseSkill(skillSlot - 1, targetAgentId); });

  const GW::SkillbarSkill &skill = skillbar->skills[skillSlot - 1];
  const GW::Skill &skilldata = GW::SkillbarMgr::GetSkillConstantData(skill.skill_id);

  if (skilldata.activation != 0) {
    float skillCastDelay = max(skilldata.activation + skilldata.aftercast, 0.25f);
    Sleep(static_cast<DWORD>(skillCastDelay * 1000));
  } else {
    float skillCastDelay = max(skilldata.activation + skilldata.aftercast, 0.15f);
    Sleep(static_cast<DWORD>(skillCastDelay * 1000));
  }

  while (skillbar->skills[skillSlot].GetRecharge() == 0 && player->GetAsAgentLiving()->GetIsCasting() && player->GetAsAgentLiving()->skill != 0) {
    Sleep(50);
    if (player->GetAsAgentLiving()->GetIsDead()) return false;
    if (isTimeElapsed(startUseSkill, timeoutInMilliSeconds)) return false;
  }
  return true;
}

uint32_t isEffectSkillOnPlayer(GW::Constants::SkillID skillId) {
  GW::Skillbar *skillbar = GW::SkillbarMgr::GetPlayerSkillbar();
  if (!skillbar || !skillbar->IsValid()) return false;

  GW::AgentEffectsArray party_effect_array = GW::Effects::GetPartyEffectArray();
  if (!party_effect_array.valid()) return false;

  const GW::EffectArray &playerEffects = party_effect_array[0].effects;
  if (!playerEffects.valid()) return false;

  uint32_t effectTimeRemaining = 0;
  for (const auto &effect : playerEffects) {
    if (static_cast<GW::Constants::SkillID>(effect.skill_id) != skillId) continue;
    if (effect.GetTimeRemaining() >= 0) {
      effectTimeRemaining = effect.GetTimeRemaining();
      break;
    }
  }

  return effectTimeRemaining;
}

bool isRecharged(uint32_t skillSlot) {
  GW::Skillbar *skillbar = GW::SkillbarMgr::GetPlayerSkillbar();
  if (!skillbar || !skillbar->IsValid()) return false;

  return skillbar->skills[skillSlot - 1].GetRecharge() <= 0;
}
