#include <gwcba/movement.h>
#include <gwcba/travel.h>

void mapTravel(GW::Constants::MapID mapId, GW::Constants::District district, uint32_t districtNumber) {
  struct MapStruct {
    GW::Constants::MapID map_id;
    int region_id;
    int language_id;
    uint32_t district_number;
  };
  auto *t = new MapStruct();
  t->map_id = mapId;
  t->district_number = districtNumber;
  t->region_id = regionFromDistrict(district);
  t->language_id = languageFromDistrict(district);

  GW::GameThread::Enqueue([t] {
    GW::UI::SendUIMessage(GW::UI::kTravel, t);
    delete t;
  });

  GW::Constants::MapID currentMapId;
  do {
    currentMapId = GW::Map::GetMapID();
    Sleep(50);
  } while (currentMapId != mapId);
}

int regionFromDistrict(GW::Constants::District district) {
  switch (district) {
    case GW::Constants::District::International:
      return GW::Constants::Region::International;
    case GW::Constants::District::American:
      return GW::Constants::Region::America;
    case GW::Constants::District::EuropeEnglish:
    case GW::Constants::District::EuropeFrench:
    case GW::Constants::District::EuropeGerman:
    case GW::Constants::District::EuropeItalian:
    case GW::Constants::District::EuropeSpanish:
    case GW::Constants::District::EuropePolish:
    case GW::Constants::District::EuropeRussian:
      return GW::Constants::Region::Europe;
    case GW::Constants::District::AsiaKorean:
      return GW::Constants::Region::Korea;
    case GW::Constants::District::AsiaChinese:
      return GW::Constants::Region::China;
    case GW::Constants::District::AsiaJapanese:
      return GW::Constants::Region::Japan;
    default:
      break;
  }
  return GW::Map::GetRegion();
}

int languageFromDistrict(GW::Constants::District district) {
  switch (district) {
    case GW::Constants::District::EuropeEnglish:
      return GW::Constants::EuropeLanguage::English;
    case GW::Constants::District::EuropeFrench:
      return GW::Constants::EuropeLanguage::French;
    case GW::Constants::District::EuropeGerman:
      return GW::Constants::EuropeLanguage::German;
    case GW::Constants::District::EuropeItalian:
      return GW::Constants::EuropeLanguage::Italian;
    case GW::Constants::District::EuropeSpanish:
      return GW::Constants::EuropeLanguage::Spanish;
    case GW::Constants::District::EuropePolish:
      return GW::Constants::EuropeLanguage::Polish;
    case GW::Constants::District::EuropeRussian:
      return GW::Constants::EuropeLanguage::Russian;
    case GW::Constants::District::AsiaKorean:
    case GW::Constants::District::AsiaChinese:
    case GW::Constants::District::AsiaJapanese:
    case GW::Constants::District::International:
    case GW::Constants::District::American:
      return 0;
    default:
      break;
  }
  return GW::Map::GetLanguage();
}

void waitForMapLoaded(GW::Constants::MapID mapId, uint32_t timeoutInMilliSeconds) {
  auto start = std::chrono::high_resolution_clock::now();
  while (true) {
    Sleep(50);
    if (isTimeElapsed(start, timeoutInMilliSeconds)) break;
    if (!GW::Map::GetIsMapLoaded()) continue;
    if (GW::Map::GetMapID() != mapId) continue;
    if (GW::Agents::GetPlayer() == nullptr) continue;
    break;
  }
}