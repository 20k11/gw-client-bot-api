#include <gwcba/faction.h>

void takeBlessing() {
    GW::WorldContext* ctx = GW::GameContext::instance()->world;
    GW::Agent *player = GW::Agents::GetPlayer();

    if (ctx->current_kurzick > ctx->current_luxon) {
        GW::Agents::SendDialog(0x85);
        GW::Agents::SendDialog(0x86);
    } else if (ctx->current_kurzick < ctx->current_luxon) {
        GW::Agents::SendDialog(0x81);
        GW::Agents::SendDialog(0x2);
        GW::Agents::SendDialog(0x84);
        GW::Agents::SendDialog(0x86);
    } else if (ctx->current_kurzick == ctx->current_luxon) {
        GW::Guild* guild = nullptr;
        const GW::GuildArray guilds = GW::GuildMgr::GetGuildArray();
        if (guilds.valid() && player->GetAsAgentLiving()->tags->guild_id < guilds.size()) {
            guild = guilds[player->GetAsAgentLiving()->tags->guild_id];
            if (!guild) return;
            if (guild->faction) { // luxon guild
                GW::Agents::SendDialog(0x81);
                GW::Agents::SendDialog(0x2);
                GW::Agents::SendDialog(0x84);
                GW::Agents::SendDialog(0x86);
            } else { // kurzick guild
                GW::Agents::SendDialog(0x85);
                GW::Agents::SendDialog(0x86);
            }

        }
    }
}
