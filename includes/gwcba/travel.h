#ifndef GWB_TRAVEL_H
#define GWB_TRAVEL_H

#include <gwcba/gwcba.h>

void mapTravel(GW::Constants::MapID mapId, GW::Constants::District district, uint32_t districtNumber);
int regionFromDistrict(GW::Constants::District _district);
int languageFromDistrict(GW::Constants::District _district);
void waitForMapLoaded(GW::Constants::MapID mapId, uint32_t timeoutInMilliSeconds = 10000);

#endif  // GWB_TRAVEL_H
