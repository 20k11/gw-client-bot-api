#ifndef GWB_SKILLBAR_H
#define GWB_SKILLBAR_H

#include <gwcba/gwcba.h>

void loadBuildTemplate(const std::string& buildTemplate);
bool useSkill(uint32_t skillSlot, GW::AgentID targetAgentId, uint32_t timeoutInMilliSeconds = 3000);
uint32_t isEffectSkillOnPlayer(GW::Constants::SkillID skillId);
bool isRecharged(uint32_t skillSlot);

#endif //GWB_SKILLBAR_H
