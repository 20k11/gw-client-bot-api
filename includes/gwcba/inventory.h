#ifndef GWB_INVENTORY_H
#define GWB_INVENTORY_H

#include <gwcba/gwcba.h>

std::vector<GW::Item*> getInventoryBagsItems();
uint32_t getInventoryTotalSlotsCount();
uint32_t getInventoryAvailableSlotsCount();

const GW::Item* getItemFromInventory(uint32_t itemModelId);
const GW::Item* getSuperiorIdentificationKitFromInventory();
const GW::Item* getSuperiorSalvageKitFromInventory();

bool buyItemFromMerchant(uint32_t itemModelId, uint32_t itemPrice);
bool buySuperiorSalvageKit();
bool buySuperiorIdentificationKit();
void buySalvageKitLogic();

bool defaultExcludeFromSell(const GW::Item* item);
void sellItemToMerchant(GW::Item* item, const std::function<bool(const GW::Item* itemToExcludeFromSell)>& excludeFromSell = defaultExcludeFromSell);

void defaultsellRuneQuotedPricePacketCallback(GW::HookStatus* hookStatus, GW::Packet::StoC::QuotedItemPrice* pak);
bool sellRuneToTrader(GW::Item* item,
                      const std::function<void(GW::HookStatus* status, GW::Packet::StoC::QuotedItemPrice* pak)>& sellRuneQuotedPricePacketCallback =
                          defaultsellRuneQuotedPricePacketCallback);
uint32_t sellRunesToRuneTrader();

bool defaultCanPickup(const GW::Item* item);
bool pickupItem(const GW::Item* item, const std::function<bool(const GW::Item* itemToPickUp)>& canPickup = defaultCanPickup);
void pickupItemsInRangeOfPlayer(float range = GW::Constants::SqrRange::Spellcast,
                                const std::function<bool(const GW::Item* itemToPickUp)>& canPickup = defaultCanPickup);

void identify(const GW::Item* item, const GW::Item* kit);
void identifyInventoryItems();

void salvageMod(const GW::Item* item, uint32_t modType);
void salvageRune(const GW::Item* item);
void salvageInsignia(const GW::Item* item);
void salvageModsFromInventoryItems();
void sellInventoryItemsToMerchant();
bool defaultCanStoreInChest(const GW::Item* item);
void storeInventoryItemsInChest(const std::function<bool(const GW::Item* itemToStore)>& canStoreInChest = defaultCanStoreInChest);

bool canStoreItemInChest(const GW::Item* item);
bool canPickUpItem(const GW::Item* item);

// Credits to GWToolbox
uint16_t moveItemToStorage(const GW::Item* item, uint16_t quantity = 1000u);
uint16_t moveItemToFirstEmptySlot(const GW::Item* item, size_t bag_first, size_t bag_last, uint16_t quantity = 1000u);
uint16_t moveMaterialToStorageTab(const GW::Item* item);
uint16_t MaxMaterialStorage();
uint16_t completeExistingItemStack(const GW::Item* item, size_t bag_first, size_t bag_last, uint16_t quantity = 1000u);
bool getPendingItemMove(uint32_t bag_idx, uint32_t slot, const std::vector<uint32_t>& pending_moves);
void setPendingMove(uint32_t bag_idx, uint32_t slot, std::vector<uint32_t>& pending_moves);

extern bool blockBeforeSalvage;
extern bool blockAfterSalvage;
extern bool blockRuneSell;
#endif  // GWB_INVENTORY_H
