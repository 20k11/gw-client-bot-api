#ifndef GWB_MOVEMENT_H
#define GWB_MOVEMENT_H

#include <gwcba/gwcba.h>

bool isTimeElapsed(std::chrono::time_point<std::chrono::steady_clock> timePoint, uint32_t timeoutInMilliSeconds);

void move(GW::GamePos gamePos);
bool moveTo(
    GW::GamePos gamePos, const std::function<bool()>& moveStepCallback = [] { return true; }, uint32_t timeoutInMilliSeconds = 60000);
void moveToZone(GW::GamePos destination, GW::Constants::MapID mapToLoad);

#endif  // GWB_MOVEMENT_H
