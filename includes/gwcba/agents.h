#ifndef GWB_AGENTS_H
#define GWB_AGENTS_H

#include <gwcba/gwcba.h>

bool isAgentMoving(const GW::Agent* agent);
bool isPlayerLoadedOnMap(GW::Constants::MapID MapId = GW::Map::GetMapID());

std::vector<GW::AgentLiving*> getAgentsLivingByAllegianceInRangeOfAgent(uint8_t allegiance, float range = GW::Constants::Range::Spellcast,
                                                                        const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentGadget*> getGadgetsInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentItem*> getAgentsItemInRangeOfAgent(float range = GW::Constants::Range::Spellcast, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::Item*> getItemsInRangeOfAgent(float range = GW::Constants::Range::Spellcast, const GW::Agent* agent = GW::Agents::GetPlayer());

std::vector<GW::AgentLiving*> getEnemiesInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentLiving*> geAlliesInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentLiving*> geNPCInRangeOfAgent(float range = GW::Constants::Range::Compass, const GW::Agent* agent = GW::Agents::GetPlayer());
std::vector<GW::AgentLiving*> geSpiritsInRangeOfAgent(float range, const GW::Agent* agent);

GW::AgentLiving* getNearestAgentLivingByAllegianceToAgent(uint8_t allegiance, const GW::Agent* agent = GW::Agents::GetPlayer());
GW::AgentGadget* getNearestGadgetAgentToAgent(const GW::Agent* agent = GW::Agents::GetPlayer());
GW::Item* getNearestItemAgentToAgent(const GW::Agent* agent = GW::Agents::GetPlayer());

bool isItemARune(const GW::Item *item);

#endif  // GWB_AGENTS_H
