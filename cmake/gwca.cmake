include_guard()
include(FetchContent)

FetchContent_Declare(
        gwca
        GIT_REPOSITORY https://github.com/GregLando113/GWCA.git
        GIT_TAG 08dcbef06569438b3906bb48b6b00b5444c1d68d
)
FetchContent_MakeAvailable(gwca)

